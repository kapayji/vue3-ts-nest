import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import MainPage from './pages/MainPage.vue';
import LeadsPage from './pages/LeadsPage.vue';
import ContactsPage from './pages/ContactsPage.vue';
import LeadDetailPage from './pages/LeadDetailPage.vue';

const routes: Array<RouteRecordRaw> = [
  // {
  //   path: '/',
  //   name: 'main',
  //   component: MainPage,
  // },
  {
    path: '/',
    redirect: '/leads',
  },
  {
    path: '/leads',
    name: 'leads',
    component: LeadsPage,
  },
  {
    path: '/leads/search',
    name: 'leads_search',
    component: LeadsPage,
  },
  {
    path: '/contacts',
    name: 'contacts',
    component: ContactsPage,
  },
  {
    path: '/leads/:id',
    name: 'lead_detail',
    component: LeadDetailPage,
  },
  {
    path: '/:pathMatch(.*)*',
    redirect: '/leads',
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
