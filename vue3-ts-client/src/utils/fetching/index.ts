export const fetchingUrl = async (link: string): Promise<[]> => {
  const response = await fetch(`${link}`);
  const data = await response.json();
  return data;
};
