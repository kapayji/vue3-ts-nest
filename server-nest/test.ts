// class Test {
//   private x: number;
//   private y: number;
//   public set _x(x: number) {
//     this.x = x;
//   }
//   public set _y(y: number) {
//     this.y = y;
//   }
//   public get _x() {
//     return this.x;
//   }
//   public get _y() {
//     return this.y;
//   }
// }
// let tt = new Test();
// tt._x = 4;
// tt._y = 2;

// console.log(`${tt._x} ${tt._y}`);
//====================
// function MyDecorator(MyClass) {
//   console.log(MyClass);
// }
// @MyDecorator
// class MyClass {}
//====================

function testFunc(
  value_A: number | string,
  value_B: number | string,
): number | string {
  if (typeof value_A === 'number' && typeof value_B === 'number') {
    return value_A * value_B;
  }
  if (typeof value_A === 'string' || typeof value_B === 'string') {
    return `${value_A}` + `${value_B}`;
  }
}
const result: number | string = testFunc(6, '4');
// console.log(result);

const testFunc_2 = (
  value_A: number | string,
  value_B: number | string,
): number | string => {
  if (typeof value_A === 'number' && typeof value_B === 'number') {
    return value_A * value_B;
  }
  if (typeof value_A === 'string' || typeof value_B === 'string') {
    return `${value_A}` + `${value_B}`;
  }
};

const result_2: number | string = testFunc(6, 3);
// console.log(result_2);

function testFunc_3(value_A: string, value_B: string): string;
function testFunc_3(value_A: number, value_B: number): number;
function testFunc_3(value_A: any, value_B: any): any {
  return value_A + value_B;
}
console.log(testFunc_3(2, 5));
console.log(testFunc_3('2', '5'));

const testFunc_4 = (greeting: string, ...names: string[]): string => {
  return greeting + ' ' + names.join(', ') + '!';
};
console.log(testFunc_4('Привет'));
const result_3 = testFunc_4('Привет', 'Вася', 'Петя', 'Стёпа');
console.log(result_3);
