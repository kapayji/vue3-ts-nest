import { Controller, Get } from '@nestjs/common';
import { ContactsService } from './contacts.service';
import { Contact } from './interface/contact.interface';

@Controller('contacts')
export class ContactsController {
  constructor(private readonly contactsService: ContactsService) {}
  @Get()
  getAll() {
    return this.contactsService.getAll();
  }
  create(contact: Contact) {
    return this.contactsService.create(contact);
  }
  updateLead(contact: Contact) {
    return this.contactsService.updateLead(contact);
  }
  findContact(contactId: any) {
    return this.contactsService.findContact(contactId);
  }
}
