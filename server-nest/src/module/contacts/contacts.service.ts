import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Contacts } from '../../entities/contacts.entity';
import { Any, Repository, UpdateResult } from 'typeorm';
import { Contact } from './interface/contact.interface';

@Injectable()
export class ContactsService {
  constructor(
    @InjectRepository(Contacts)
    private readonly contactsRepository: Repository<Contacts>,
  ) {}
  async getAll(): Promise<Contact[]> {
    return await this.contactsRepository.find({
      relations: ['contactsId', 'contactsId.leadId'],
    });
  }
  async getAllContactsToLeads(): Promise<Contact[]> {
    return await this.contactsRepository.find();
  }
  async findContact(contactId: any): Promise<Contact[]> {
    return await this.contactsRepository.find({ externalId: Any([contactId]) });
  }
  async create(contact: Contact): Promise<Contact | UpdateResult> {
    const isExist = await this.checkExistedLead(contact);
    if (isExist) {
      return await this.updateLead(contact);
    }
    return await this.contactsRepository.save(contact);
  }
  async updateLead(contact: Contact): Promise<UpdateResult> {
    return await this.contactsRepository.update(
      { externalId: contact.externalId },
      contact,
    );
  }
  async checkExistedLead(contact: Contact): Promise<boolean> {
    const isExist = await this.contactsRepository.find({
      externalId: contact.externalId,
    });
    if (isExist.length) {
      return true;
    }
    return false;
  }
}
