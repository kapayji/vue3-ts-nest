export interface Contact {
  id?: number;
  externalId: number;
  name: string;
  customFields?: any[];
  contactsId?: any[];
}
