import { Test, TestingModule } from '@nestjs/testing';
import { LeadsContactsController } from './leads-contacts.controller';

describe('LeadsContactsController', () => {
  let controller: LeadsContactsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [LeadsContactsController],
    }).compile();

    controller = module.get<LeadsContactsController>(LeadsContactsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
