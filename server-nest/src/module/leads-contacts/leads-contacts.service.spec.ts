import { Test, TestingModule } from '@nestjs/testing';
import { LeadsContactsService } from './leads-contacts.service';

describe('LeadsContactsService', () => {
  let service: LeadsContactsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LeadsContactsService],
    }).compile();

    service = module.get<LeadsContactsService>(LeadsContactsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
