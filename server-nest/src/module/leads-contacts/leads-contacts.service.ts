import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Leads_Contacts } from 'src/entities/leads_contacts.entity';
import { Repository } from 'typeorm';
import { ContactsService } from '../contacts/contacts.service';
import { LeadsService } from '../leads/leads.service';

@Injectable()
export class LeadsContactsService {
  constructor(
    @InjectRepository(Leads_Contacts)
    private readonly leadsContactsRepository: Repository<Leads_Contacts>,
    @Inject(forwardRef(() => ContactsService))
    private readonly contactsService: ContactsService,
    @Inject(forwardRef(() => LeadsService))
    private readonly leadsService: LeadsService,
  ) {}
  async setRelationsLeadsWithContacts(
    externalId: number,
    contactsId: number[],
  ): Promise<void> {
    const lead = await this.leadsService.getOneLead(externalId);
    const contacts = (await this.contactsService.findContact(contactsId))
      .map(({ id }) => Object.values({ id }))
      .flat();
    await this.deleteDeprecationRelations(lead.id, contacts);
    for (const contactId of contacts) {
      const newLead = {
        leadId: lead.id,
        contactId: contactId,
      };
      const existRelation = await this.leadsContactsRepository.find({
        where: [{ leadId: lead.id, contactId }],
      });
      if (!existRelation.length) {
        await this.leadsContactsRepository.save(newLead);
      }
    }
  }
  async deleteDeprecationRelations(
    leadId: number,
    contacts: number[],
  ): Promise<void> {
    const allExistRelations = await this.leadsContactsRepository.find({
      where: { leadId },
    });
    for (const exRe of allExistRelations) {
      for (const contactId of contacts) {
        if (exRe.contactId !== contactId) {
          await this.leadsContactsRepository.delete({ id: exRe.id });
        }
      }
    }
  }
}
