import { Controller } from '@nestjs/common';
import { LeadsContactsService } from './leads-contacts.service';

@Controller()
export class LeadsContactsController {
  constructor(private readonly leadsContactsService: LeadsContactsService) {}
  setRelationsLeadsWithContacts(externalId: number, contactsId: number[]) {
    return this.leadsContactsService.setRelationsLeadsWithContacts(
      externalId,
      contactsId,
    );
  }
  deleteDeprecationRelations(leadId: number, contacts: number[]) {
    return this.leadsContactsService.deleteDeprecationRelations(
      leadId,
      contacts,
    );
  }
}
