import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Leads_Contacts } from 'src/entities/leads_contacts.entity';
import { ContactsModule } from '../contacts/contacts.module';
import { LeadsModule } from '../leads/leads.module';
import { LeadsContactsController } from './leads-contacts.controller';
import { LeadsContactsService } from './leads-contacts.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Leads_Contacts]),
    forwardRef(() => LeadsModule),
    forwardRef(() => ContactsModule),
  ],
  providers: [LeadsContactsService],
  controllers: [LeadsContactsController],
  exports: [LeadsContactsService],
})
export class LeadsContactsModule {}
