import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import axios from 'axios';
import { Model } from 'mongoose';
import { Token, TokenDocument } from '../../entities/token.entity';
import { TokenObject } from './interface/token.interface';

@Injectable()
export class TokenService {
  constructor(
    private readonly jwtService: JwtService,
    @InjectModel(Token.name) private tokenModel: Model<TokenDocument>,
  ) {}
  async getAccessToken(): Promise<string> {
    const token: TokenObject = await this.tokenModel.findOne();
    const accessToken = token.access_token;
    const decode = this.jwtService.decode(accessToken);
    const isTokenExpired = Date.now() >= decode['exp'] * 1000;
    if (isTokenExpired) {
      const newTokenObj: TokenObject = await this.refreshToken(token);
      token.access_token = newTokenObj.access_token;
      token.refresh_token = newTokenObj.refresh_token;
      await token.save();
      return token.access_token;
    }
    return token.access_token;
  }
  testFunc() {
    console.log('Token Module');
  }
  async refreshToken(refreshObj: TokenObject): Promise<TokenObject> {
    const data = {
      client_id: refreshObj.client_id,
      client_secret: refreshObj.client_secret,
      grant_type: 'refresh_token',
      refresh_token: refreshObj.refresh_token,
      redirect_uri: refreshObj.redirect_uri,
    };
    const newToken: TokenObject = await axios
      .post('https://gexepoh816.amocrm.ru/oauth2/access_token', data)
      .then((response) => response.data);
    return newToken;
  }
}
