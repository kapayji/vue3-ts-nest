export interface TokenObject {
  _id?: any;
  expires_in?: number;
  token_type?: string;
  redirect_uri?: string;
  client_id?: string;
  access_token: string;
  refresh_token: string;
  client_secret?: string;
  grant_type?: string;
  save?: () => void;
}
