import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';
import { ContactsService } from '../contacts/contacts.service';
import { Contact } from '../contacts/interface/contact.interface';
import { LeadsContactsService } from '../leads-contacts/leads-contacts.service';
import { Lead } from '../leads/interface/lead.interface';
import { LeadsService } from '../leads/leads.service';
import { TokenService } from '../token/token.service';
import { ContactsAmo } from './interface/contacts-amo.interface';
import { LeadsAmo } from './interface/leads-amo.interface';
import { Status } from './interface/status.interface';

@Injectable()
export class LeadsAndContactsAmoService {
  constructor(
    private readonly tokenService: TokenService,
    @Inject(forwardRef(() => LeadsService))
    private readonly leadsService: LeadsService,
    @Inject(forwardRef(() => ContactsService))
    private readonly contactService: ContactsService,
    @Inject(forwardRef(() => LeadsContactsService))
    private readonly leadsContactsService: LeadsContactsService,
  ) {
    this.api = axios.create({
      baseURL: process.env.AMO_BASE_URL,
      headers: {
        'Content-type': 'application/json',
      },
    });
    this.api.interceptors.request.use(
      async (config: AxiosRequestConfig) => {
        const access_token = await this.tokenService.getAccessToken();
        config.headers.common['Authorization'] = `Bearer ${access_token}`;
        return config;
      },
      (err) => Promise.reject(err),
    );
  }

  private api: AxiosInstance;

  @Cron('*/5 * * * *')
  async leadsAndContactsFromAmo(): Promise<void> {
    await this.getContactsFromAmo();
    await this.getLeadsFromAmo();
  }
  //Забираем сделки и сразу обрабатываем под нужный нам вид
  async getLeadsFromAmo(): Promise<void> {
    const leadsFromAmo: LeadsAmo = await this.api
      .get('/leads?with=contacts')
      .then((response) => response.data._embedded);
    const users = await this.api
      .get('/users')
      .then((response) => response.data._embedded.users);
    const pipelines = await this.api
      .get(`/leads/pipelines`)
      .then((response) => response.data._embedded.pipelines);
    for (const leadFromAmo of leadsFromAmo.leads) {
      const creatingDate: string = new Date(
        leadFromAmo.created_at * 1000,
      ).toLocaleDateString();
      const status: Status = pipelines
        .find(({ id }: { id: number }) => id === leadFromAmo.pipeline_id)
        ._embedded.statuses.find(
          ({ id }: { id: number }) => id === leadFromAmo.status_id,
        );
      const user: string = users.find(
        (user: { id: number }) => user.id === leadFromAmo.responsible_user_id,
      ).name;
      const contacts = leadFromAmo._embedded.contacts
        .map(({ id }) => Object.values({ id }))
        .flat();
      const lead: Lead = {
        externalId: leadFromAmo.id,
        name: leadFromAmo.name,
        budget: leadFromAmo.price,
        status: {
          statusName: status.name,
          statusColor: status.color,
        },
        user,
        creatingDate,
      };
      await this.leadsService.create(lead);
      if (contacts.length) {
        await this.leadsContactsService.setRelationsLeadsWithContacts(
          leadFromAmo.id,
          contacts,
        );
      }
    }
  }
  //Забираем контакты и сразу обрабатываем под нужный нам вид
  async getContactsFromAmo(): Promise<void> {
    const contactsFromAmo: ContactsAmo = await this.api
      .get('/contacts')
      .then((response) => response.data._embedded);
    for (const contactFromAmo of contactsFromAmo.contacts) {
      const customFields = (contactFromAmo.custom_fields_values || [])
        .filter(({ field_code }) => ['PHONE', 'EMAIL'].includes(field_code))
        .map(({ field_code, values }) => ({
          field_code,
          values: values.map(({ value }) => ({
            value,
          })),
        }));
      const contact: Contact = {
        externalId: contactFromAmo.id,
        name: contactFromAmo.name,
        customFields,
      };
      await this.contactService.create(contact);
    }
  }
}
