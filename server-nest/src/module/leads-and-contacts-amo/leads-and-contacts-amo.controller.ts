import { Controller } from '@nestjs/common';
import { LeadsAndContactsAmoService } from './leads-and-contacts-amo.service';

@Controller()
export class LeadsAndContactsAmoController {
  constructor(
    private readonly leadsAndContactsAmoService: LeadsAndContactsAmoService,
  ) {}
  leadsAndContactsFromAmo() {
    this.leadsAndContactsAmoService.leadsAndContactsFromAmo();
  }
  getLeadsFromAmo() {
    this.leadsAndContactsAmoService.getLeadsFromAmo();
  }
  getContactsFromAmo() {
    this.leadsAndContactsAmoService.getContactsFromAmo();
  }
}
