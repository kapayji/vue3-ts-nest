export interface ContactsAmo {
  contacts: [
    {
      id: number;
      name: string;
      custom_fields_values: [
        { field_code: string; values: [{ value: string }] },
      ];
    },
  ];
}
