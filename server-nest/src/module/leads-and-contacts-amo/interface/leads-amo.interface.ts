export interface LeadsAmo {
  leads: [
    {
      id: number;
      name: string;
      price: number;
      created_at: number;
      status_id: number;
      responsible_user_id: number;
      pipeline_id: number;
      _embedded: {
        contacts?: [
          {
            id: number;
          },
        ];
      };
    },
  ];
}
