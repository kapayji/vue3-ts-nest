import { Test, TestingModule } from '@nestjs/testing';
import { LeadsAndContactsAmoController } from './leads-and-contacts-amo.controller';
import { LeadsAndContactsAmoService } from './leads-and-contacts-amo.service';

describe('LeadsAndContactsAmoService', () => {
  let service: LeadsAndContactsAmoService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LeadsAndContactsAmoService],
      controllers: [LeadsAndContactsAmoController],
    }).compile();

    service = module.get<LeadsAndContactsAmoService>(
      LeadsAndContactsAmoService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
