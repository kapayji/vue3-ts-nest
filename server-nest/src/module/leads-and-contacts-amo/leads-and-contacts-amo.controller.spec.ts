import { Test, TestingModule } from '@nestjs/testing';
import { LeadsAndContactsAmoController } from './leads-and-contacts-amo.controller';
import { LeadsAndContactsAmoService } from './leads-and-contacts-amo.service';

describe('LeadsAndContactsAmoController', () => {
  let controller: LeadsAndContactsAmoController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LeadsAndContactsAmoService],
      controllers: [LeadsAndContactsAmoController],
    }).compile();

    controller = module.get<LeadsAndContactsAmoController>(
      LeadsAndContactsAmoController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
