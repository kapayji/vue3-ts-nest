import { forwardRef, Module } from '@nestjs/common';
import { ContactsModule } from '../contacts/contacts.module';
import { LeadsContactsModule } from '../leads-contacts/leads-contacts.module';
import { LeadsModule } from '../leads/leads.module';
import { TokenModule } from '../token/token.module';
import { LeadsAndContactsAmoController } from './leads-and-contacts-amo.controller';
import { LeadsAndContactsAmoService } from './leads-and-contacts-amo.service';

@Module({
  imports: [
    TokenModule,
    forwardRef(() => LeadsModule),
    forwardRef(() => ContactsModule),
    forwardRef(() => LeadsContactsModule),
  ],
  controllers: [LeadsAndContactsAmoController],
  providers: [LeadsAndContactsAmoService],
  exports: [LeadsAndContactsAmoService],
})
export class LeadsAndContactsAmoModule {}
