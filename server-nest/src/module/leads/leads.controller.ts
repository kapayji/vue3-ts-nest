import { Controller, Get, Param, Query } from '@nestjs/common';
import { query } from 'express';
import { Lead } from './interface/lead.interface';
import { LeadsService } from './leads.service';

@Controller('leads')
export class LeadsController {
  constructor(private readonly leadsService: LeadsService) {}

  @Get()
  getAll() {
    return this.leadsService.getAll();
  }
  @Get('search')
  getLeadByName(@Query() query) {
    console.log('controller:' + query.name);
    return this.leadsService.getLeadByName(query);
  }

  create(lead: Lead) {
    return this.leadsService.create(lead);
  }
  updateLead(lead: Lead) {
    return this.leadsService.updateLead(lead);
  }
  getAllLeadsToContacts() {
    return this.leadsService.getAllLeadsToContacts();
  }
  @Get(':id')
  getLeadById(@Param() params) {
    return this.leadsService.getLeadById(params.id);
  }
  getOneLead(externalId: number) {
    return this.leadsService.getOneLead(externalId);
  }
}
