export interface Lead {
  id?: number;
  externalId: number;
  name: string;
  budget: number;
  user: string;
  creatingDate: string;
  status: { statusName: string; statusColor: string };
  leadId?: number;
}
