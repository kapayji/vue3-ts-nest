import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ILike, Repository, UpdateResult } from 'typeorm';
import { Leads } from '../../entities/leads.entity';
import { Lead } from './interface/lead.interface';

@Injectable()
export class LeadsService {
  constructor(
    @InjectRepository(Leads)
    private readonly leadsRepository: Repository<Leads>,
  ) {}
  async getAll(): Promise<Leads[]> {
    return await this.leadsRepository.find({
      relations: ['leadsId', 'leadsId.contactId'],
    });
  }
  async getAllLeadsToContacts(): Promise<Lead[]> {
    return await this.leadsRepository.find();
  }
  async getLeadByName(query): Promise<Lead[]> {
    return await this.leadsRepository.find({
      where: { name: ILike('%' + query.name + '%') },
      relations: ['leadsId', 'leadsId.contactId'],
    });
  }
  async getLeadById(id: number): Promise<Lead[]> {
    return await this.leadsRepository.find({
      where: { id },
      relations: ['leadsId', 'leadsId.contactId'],
    });
  }
  async getOneLead(externalId: number): Promise<Lead> {
    return await this.leadsRepository.findOne({ externalId });
  }
  async create(lead: Lead): Promise<Lead | UpdateResult> {
    const isExist = await this.checkExistedLead(lead);
    if (isExist) {
      return this.updateLead(lead);
    }
    return this.leadsRepository.save(lead);
  }
  async updateLead(lead: Lead): Promise<UpdateResult> {
    return await this.leadsRepository.update(
      { externalId: lead.externalId },
      lead,
    );
  }
  async checkExistedLead(lead: Lead): Promise<boolean> {
    const isExist = await this.leadsRepository.find({
      externalId: lead.externalId,
    });
    if (isExist.length) {
      return true;
    }
    return false;
  }
}
