import { JwtService } from '@nestjs/jwt';
import { Model } from 'mongoose';
import { TokenMiddleware } from './token.middleware';

describe('TokenMiddleware', () => {
  it('should be defined', () => {
    expect(new TokenMiddleware()).toBeDefined();
  });
});
