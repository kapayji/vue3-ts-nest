import {
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm/index';
import { Contacts } from './contacts.entity';
import { Leads_Contacts } from './leads_contacts.entity';

@Entity()
export class Leads {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  externalId: number;

  @Column()
  name: string;

  @Column()
  budget: number;

  @Column()
  user: string;

  @Column()
  creatingDate: string;

  @Column('simple-json')
  status: { statusName: string; statusColor: string };

  // @Column('jsonb')
  // contactsId?: [{ id: number }];

  // @ManyToMany(() => Contacts)
  // @JoinTable({
  //   name: 'leads_contacts',
  // })
  // contacts?: Contacts[];
  @OneToMany(() => Leads_Contacts, (leads_contacts) => leads_contacts.leadId)
  leadsId?: Leads_Contacts[];
}
