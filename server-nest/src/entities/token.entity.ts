import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type TokenDocument = Token & Document;

@Schema()
export class Token {
  @Prop()
  expires_in: number;

  @Prop()
  token_type: string;

  @Prop()
  redirect_uri: string;

  @Prop()
  client_id: string;

  @Prop()
  access_token: string;

  @Prop()
  refresh_token: string;

  @Prop()
  client_secret: string;
}

export const TokenSchema = SchemaFactory.createForClass(Token);
