import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';
import { Contacts } from './contacts.entity';
import { Leads } from './leads.entity';

@Entity()
export class Leads_Contacts {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Leads, (lead) => lead.id)
  @JoinColumn([{ name: 'leadId' }, { name: 'id' }])
  leadId: number;

  @ManyToOne(() => Contacts, (contact) => contact.id)
  @JoinColumn([{ name: 'contactId' }, { name: 'id' }])
  contactId: number;
}
