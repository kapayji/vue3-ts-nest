import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm/index';
import { Leads } from './leads.entity';
import { Leads_Contacts } from './leads_contacts.entity';

@Entity()
export class Contacts {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column()
  externalId: number;

  @Column()
  name: string;

  @Column('jsonb')
  customFields?: any[];

  @OneToMany(() => Leads_Contacts, (leads_contacts) => leads_contacts.contactId)
  contactsId?: Leads_Contacts[];
}
