import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { LeadsModule } from './module/leads/leads.module';
import { ConfigModule } from '@nestjs/config';
import { ContactsModule } from './module/contacts/contacts.module';
import { MongooseModule } from '@nestjs/mongoose';
import { TokenModule } from './module/token/token.module';
import { TokenMiddleware } from './middleware/token.middleware';
import { LeadsAndContactsAmoModule } from './module/leads-and-contacts-amo/leads-and-contacts-amo.module';
import { ScheduleModule } from '@nestjs/schedule';
import { LeadsContactsModule } from './module/leads-contacts/leads-contacts.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    ScheduleModule.forRoot(),
    LeadsModule,
    ConfigModule.forRoot({ envFilePath: '.yakovlev.env' }),
    ContactsModule,
    MongooseModule.forRootAsync({
      useFactory: () => ({
        uri: process.env.MONGODB_URI,
      }),
    }),
    TokenModule,
    LeadsAndContactsAmoModule,
    LeadsContactsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  constructor(private connection: Connection) {}
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(TokenMiddleware).forRoutes('/');
  }
}
